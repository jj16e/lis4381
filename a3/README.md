
# LIS4381 - Mobile Web Application Development

## J'tierra Jones	

### Assignment 3 Requirements:

1. Create ERD with MySQL Workbench
2. Forward engineer with 10 records in each table
3. Create Event app and launcher icon image
4. Display icon image in activity
5. Add color(s) to activity controls 
6. Add border around image and button
7. Add text shadow to button 

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running applications first user interface
* Screenshot of running applications second user interface
* Links to following files
	* a3.mwb
	* a3.sql
* Screenshots of skillsets 4, 5 & 6
		 		

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD screenshot](img/ERD.png)

*Screenshot of My Event App*:

|App Screen 1 | App Screen 2|
|--------------|-----------------------|
|![My Event UI 1](img/app1.png)|![My Event UI 2](img/app2.png)|


*Screenshot of Skillsets 4, 5, & 6*:

| Skillset #4 | Skillset #5| Skillset #6| 
|--------------|-------------|-------------|
| ![Skillset 4](img/ss4.png)| ![Skillset 5](img/ss5.png)| ![Skillset 6](img/ss6.png) |


#### Assignment Links:

*a3.mwb:*
[a3.mwb Link](docs/a3.mwb)

*a3.sql*
[a3.sql Link](docs/a3.sql)
