
# LIS 4381

## J'tierra Jones

### Project 1 Requirements:

*Six Parts*

1. Create a launcher icon image and display in activites.
2. Add background color to both activites
3. Add border image 
4. Add text shadow to button
5. Chapter Questions 
6. Skillset 7-9

#### README.md file should include the following items:

* Screenshot of application running first and second user interface
* Screenshot of Skill Sets


#### Assignment Screenshots:

*Screenshot of My Business Card*:

|App Main Page | App Details page|
|--------------|-----------------------|
|![My Card 1](img/app1.png)|![My Card 2](img/app2.png)|

*Screenshot of Skillsets 7, 8, & 9*:

| Skillset #7 | Skillset #8| Skillset #9| 
|--------------|-------------|-------------|
| ![Skillset 7](img/ss7.png)| ![Skillset 8](img/ss8.png)| ![Skillset 9](img/ss9.png) |
