
# LIS4381

## J'tierra Jones

### Assignment 5 Requirements:

*Four Parts:*

1. Online Portfolio link
2. Develop server-side validation
3. Chapter Questions
4. Skill sets 13-15

#### README.md file should include the following items:

* Screenshot of Online Portfolio
* Screenshot of Pet store server-side validation
* Screenshots of java skill sets
* Link to Online portfolio


#### Assignment Screenshots:


*Screenshot of Assignment 5*

|Assignment 5| 
|--------------|
|![Assignment5](img/index.png)|

| Server-Side Validation|
|-----------------------|
|![Failed](img/fail.png)|


*Screenshot of Skillsets 13, 14 & 15*:

|Skillset #13:  |
|--------------|
|![Skillset 13](img/ss13.png)|

|Skillset #14: A | Skillset #14: B|
|--------------|-----------------------|
|![Skillset 14 A](img/a.png)|![Skillset 14 B](img/b.png)|

|Skillset #14: C | Skillset #14: D|
|--------------|-----------------------|
|![SKillset 14 C](img/c.png)|![Skillset 14 D](img/d.png)|

|Skillset #15: A | Skillset #15: B|
|--------------|-----------------------|
|![SKillset 15 A](img/ss15.png)|![Skillset 15 B](img/s15.png)|



#### Assigntment Link:

[Online Portfolio](http://localhost/repos/lis4381/index.php "online portfolio")
