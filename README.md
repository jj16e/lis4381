> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## J'tierra Jones

### LIS4381 Requirements

*Course Work Links*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My first App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android App
    - Provide screenshots of completed app
    - Provide screenshots of skill sets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD with MySQL Workbench
    	- Add 10 records to tables
    - Provide screenshots of first and second user interface
    - Provide links to following files:
    	- a3.mwb
    	- a3.sql

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Create Online Portfolio
	- Provide screenshots of passed and failed validation
	- Skill Sets 10-12

5. [A5 README.md](a5/README.md "My A5 README.md file")
     - Provide screenshots of server-side validation and error
    - Provide link to online portfolio
    - Provide screenshots of skill sets 13-15

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a mobile app personal business card
    - Provide screenshots of business card first and second user interface
    - Provide screenshots of skill sets

7. [P2 README.md](p2/README.md "My P2 README.md file")
