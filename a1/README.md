> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## J'tierra Jones

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket 
2. Development Installations
3. Questions 
4. Bitbucket repo links:
    a) this assignment and 
    b) the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of ampps installation running
* Screenshot of running JDK java Hello
* git commands w/short descriptions
* Bitbucket repo links: a)this assignment, and the completed tutorial repo above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - used to generate new or empty Git repository
2. git status - will list status of files that have been changed and specify what might files still need to be added
3. git add - used to add specific files and/or to add changes that have been made
4. git commit - this is used to commit file changes locally
5. git push - used to push changes that have been made to remote repository
6. git pull - is used to fetch/merge changes between repositories 
7. git checkout - used to switch between branches 


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jj16e/bitbucketstationlocationss/ "Bitbucket Station Locations")

