
# LIS4381

## J'tierra Jones

### Assignment 4 Requirements:

*Two Parts:*

1. Create online portfolio 
2. Skill Sets

#### README.md file should include the following items:

* Bullet-list items
* Failed Validation Bootstrap
* Passed Validation Bootstrap
* Link to online portfolio
* Skill Sets 10-12


#### Assignment Screenshots:

*Screenshot of My Online Portfolio*:

|Main Page | Failed Validation| Passed validation
|--------------|-----------------------|----------|
|![Main Page](img/home.png)|![Failed](img/failed.png)|![passed](img/passed.png) |

*Screenshot of Skillsets 10, 11, & 12*:

| Skillset #10 | Skillset #11| Skillset #12| 
|--------------|-------------|-------------|
| ![Skillset 10](img/ss10.png)| ![Skillset 11](img/ss11.png)| ![Skillset 12](img/ss12.png) |


#### Assignment Link:
[Online Portfolio](http://localhost/repos/lis4381/index.php "online portfolio")