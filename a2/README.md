
# LIS 4381

## J'tierra Jones 

### Assignment 2 Requirements:

*Three Parts:*

1. Create a mobile recipe app using Android Studio
2. Provide screenshots of completed App
3. Provide screenshots of skill sets

#### README.md file should include the following items:

* Course title, your name, assignment requirements 
* Screenshot of running applications first user interface
* Screenshot of running applications second user interface

#### Assignment Screenshots:

|  App Screen 1  |  App Screen 2  |
|----------------|----------------|
|![app page 1](img/app1.png)      | ![app page 2](img/app2.png)



| Skill Set #1  | Skill Set #2| 
|--------|--------|
|![skill set1](img/EvenorOdd.png)|![skill set2](img/largestNumber.png)|

|Skill Set #3|
|------------|
![skill set 3](img/ArraysandLoops.png)|

#### Links:

*Assignment Links:*
[A1 link](https://bitbucket.org/jj16e/lis4381/src/master/a1/ "Assignment 1 link")
